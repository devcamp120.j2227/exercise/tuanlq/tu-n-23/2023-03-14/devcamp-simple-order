import Card from "./components/Card";
import Card1 from "./components/Card1";
import Card2 from "./components/Card2";
import Card3 from "./components/Card3";


const routeList = [
    {path:"/", element: <Card />},
    {label: "Product1", path:"/Product1/1", element: <Card1/>},
    {label: "Product2", path:"/Product2/2", element: <Card2/>},
    {label: "Product3", path:"/Product3/3", element: <Card3/>},
]
export default routeList;