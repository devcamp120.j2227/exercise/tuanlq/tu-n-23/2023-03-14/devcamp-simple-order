import {Route, Routes} from "react-router-dom";
import Card from "../components/Card";
import routeList from "../routes";

export default function Body(){
    return(
        <div>
            <Routes>
                {
                    routeList.map((route, index) => {
                        if(route.path){
                            return <Route path={route.path} element={route.element} key={index}></Route>
                        }else{
                            return null
                        }
                    })
                }
                <Route path="*" element={<Card/>}></Route>
            </Routes>
        </div>
    )
}