import { CHANGE_QUANTITY_ADD_TOTAL ,
  ON_CLICK_BUY_ORDER1,
  ON_CLICK_BUY_ORDER2,
  ON_CLICK_BUY_ORDER3

} from "../constants/card.constants";

const mobile = {
    value: 0,
    total:0,
    mobileList: [
    {
      id:1,
      name: "IPhone X",
      price: 900,
      quantity: 0
    },
    {
      id:2,
      name: "Samsung S9",
      price: 800,
      quantity: 0
    },
    {
      id:3,
      name: "Nokia 8",
      price: 650,
      quantity: 0
    }
]   ,
    
}

const cardReducer = (state = mobile, action) => {
    switch (action.type) {
        case CHANGE_QUANTITY_ADD_TOTAL:
           state.mobileList[action.payload].quantity++
            break;
        case ON_CLICK_BUY_ORDER1:
            state.mobileList[0].quantity++;
            console.log(state.mobileList[0]);
            state.total += state.mobileList[0].price;
            break;
        case ON_CLICK_BUY_ORDER2:
          
            state.mobileList[1].quantity++;
            console.log(state.mobileList[1]);
            state.total += state.mobileList[1].price;
            break;
        case ON_CLICK_BUY_ORDER3:
          
            state.mobileList[2].quantity++;
            console.log(state.mobileList[2]);
            state.total += state.mobileList[2].price;
            break;
        default:
            break;
    }
    return { ...state };
}

export default cardReducer;