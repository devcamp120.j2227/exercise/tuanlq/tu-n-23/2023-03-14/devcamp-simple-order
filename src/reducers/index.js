//Root reducer(Đây là nơi chứa reducer chính)
import { combineReducers } from "redux"; //thư viện để tạo reducer

import cardReducer from "./card.reducer";

const rootReducer = combineReducers({
    cardReducer
});

export default rootReducer;