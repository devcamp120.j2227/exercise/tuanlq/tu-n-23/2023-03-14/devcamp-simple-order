import {  Grid, Container } from "@mui/material";

import Title from "./Order/title";
import Order1 from "./Order/OrderChild/Order1";
import { useLocation } from "react-router-dom";
const Card1 = () => {
    const location = useLocation();
    console.log(location.pathname.split("/")[2]);
    return(
        <>
        <Title></Title>
        <Container sx={{ border: "1px solid black"}}>
            <Grid  container  direction="row" alignItems="center" sx= {{ display: "flex", justifyContent:"space-between"}}>
                <Order1 prosCardWidth ="1150px" propsBackDisplay={"block"} propsBuyDisplay={"none"} propsDetailDisplay={"none"}></Order1>
            </Grid>
        </Container>
        </>
    )
}

export default Card1