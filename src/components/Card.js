import {  Grid, Container } from "@mui/material";

import Title from "./Order/title";
import Order1 from "./Order/OrderChild/Order1";
import Order2 from "./Order/OrderChild/Order2";
import Order3 from "./Order/OrderChild/Order3";
import Label from "./Order/label";

const Card = () => {
    return(
        <>
        <Title></Title>
        <Container sx={{ border: "1px solid black"}}>
            <Grid  container  direction="row" alignItems="center" sx= {{ display: "flex", justifyContent:"space-between"}}>
                <Order1 prosCardWidth ="300px" propsBackDisplay={"none"} ></Order1>
                <Order2 prosCardWidth ="300px" propsBackDisplay={"none"} ></Order2>
                <Order3 prosCardWidth ="300px" propsBackDisplay={"none"} ></Order3>
            </Grid>
            <Label></Label>
        </Container>
        </>
    )
}

export default Card