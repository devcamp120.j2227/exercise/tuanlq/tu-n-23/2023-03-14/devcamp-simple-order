import {  Grid, Container } from "@mui/material";

import Title from "./Order/title";
import Order3 from "./Order/OrderChild/Order3";
import { useLocation } from "react-router-dom";
const Card3 = () => {
    const location = useLocation();
    console.log(location.pathname.split("/")[2]);
    return(
        <>
        <Title></Title>
        <Container sx={{ border: "1px solid black"}}>
            <Grid  container  direction="row" alignItems="center" sx= {{ display: "flex", justifyContent:"space-between"}}>
                <Order3 prosCardWidth ="1150px"  propsBackDisplay={"block"} propsBuyDisplay={"none"} propsDetailDisplay={"none"}></Order3>
            </Grid>
        </Container>
        </>
    )
}

export default Card3