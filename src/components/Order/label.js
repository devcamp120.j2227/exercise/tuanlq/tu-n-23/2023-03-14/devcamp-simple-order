import { useDispatch, useSelector } from "react-redux"
export default function Label (){
    const dispatch = useDispatch();
    const  {total} = useSelector(data => data.cardReducer);
    return(
        <h3 >Total: {total} USD </h3>
    )
}