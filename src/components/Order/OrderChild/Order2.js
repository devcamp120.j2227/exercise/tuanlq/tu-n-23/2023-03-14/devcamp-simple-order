import { useDispatch, useSelector } from "react-redux"
import {  Button, Grid,Typography, CardContent, Card } from "@mui/material";
import { changeQuantityAndTotalOrder2 } from "../../../action/card.action";
import { useNavigate } from "react-router-dom";
export default  function Order2 ({prosCardWith, propsBuyDisplay, propsDetailDisplay,propsBackDisplay}){
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {mobileList} = useSelector(reduxdata => reduxdata.cardReducer);
    const OnClickOrder2 = () => {
        dispatch(changeQuantityAndTotalOrder2());
    }
    const handlerDetail = () => {
        navigate("/Product2/2");
     }
     const handlerBack = () => {
        navigate("/");
    }
    return(

            <Grid item  xs={3} md={3} lg={3} sm={3}  mt={3} mb={3}>
                <Card sx={{ width: prosCardWith }}>
                    <CardContent>
                        <Typography variant="h5" component="div">
                        {mobileList[1].name}
                        </Typography>
                        <br/>
                        <Typography sx={{ m: 1.5 }} color="text.secondary">
                        Price: {mobileList[1].price}
                        </Typography>
                        <Typography sx={{ m: 1.5 }} color="text.secondary">
                        Quantity: {mobileList[1].quantity}
                        </Typography>
                        <Typography variant="body2">
                            <Button sx={{display: propsBuyDisplay , margin: "4px"}} variant="contained"size="small" onClick={() => OnClickOrder2()}>Buy</Button>
                            <Button sx={{display: propsDetailDisplay, margin: "4px"}} variant="contained"size="small" onClick={handlerDetail} >Detail</Button>
                            <Button sx={{display:propsBackDisplay , margin: "4px"}} variant="contained"size="small"  onClick={handlerBack} >Back</Button>
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
    
    )
}