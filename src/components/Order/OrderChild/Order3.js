import { useSelector, useDispatch} from "react-redux"
import {  Button, Grid,Typography, CardContent, Card } from "@mui/material";
import { changeQuantityAndTotalOrder3 } from "../../../action/card.action";
import { useNavigate } from "react-router-dom";
export default  function Order3 ({ prosCardWith, propsBuyDisplay, propsDetailDisplay,propsBackDisplay}){
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const {mobileList} = useSelector(reduxdata => reduxdata.cardReducer);
    const OnClickOrder3 = () => {
       dispatch(changeQuantityAndTotalOrder3());
    }
    const handlerDetail = () => {
        navigate("/Product3/3");
     }
     const handlerBack = () => {
        navigate("/");
    }
    return(
    
            <Grid item  xs={3} md={3} lg={3} sm={3}  mt={3} mb={3}>
                <Card sx={{ width: prosCardWith}}>
                    <CardContent>
                        <Typography variant="h5" component="div">
                        {mobileList[2].name}
                        </Typography>
                        <br/>
                        <Typography sx={{ m: 1.5 }} color="text.secondary">
                        Price: {mobileList[2].price}
                        </Typography>
                        <Typography sx={{ m: 1.5 }} color="text.secondary">
                        Quantity: {mobileList[2].quantity}
                        </Typography>
                        <Typography variant="body2">
                            <Button sx={{display: propsBuyDisplay}} variant="contained"size="small" onClick={() => OnClickOrder3()}>Buy</Button>
                            <Button sx={{display: propsDetailDisplay , margin: "4px"}} variant="contained"size="small"  onClick={handlerDetail}>Detail</Button>
                            <Button  sx={{display:propsBackDisplay , margin: "4px"}} variant="contained"size="small"   onClick={handlerBack}>Back</Button>
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
     
    )
}