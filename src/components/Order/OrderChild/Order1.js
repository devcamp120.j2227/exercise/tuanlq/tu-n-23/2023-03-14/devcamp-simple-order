import { useSelector, useDispatch } from "react-redux"
import {  Button, Grid,Typography, CardContent, Card } from "@mui/material";
import { changeQuantityAndTotalOrder1 } from "../../../action/card.action";
import { useNavigate, useLocation } from "react-router-dom";

export default  function Order1 ({prosCardWidth, propsBuyDisplay, propsDetailDisplay, propsBackDisplay, propsRouteList}){
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const {mobileList} = useSelector(reduxdata => reduxdata.cardReducer);
    const ChangeonClickOrder1 = () => {
        dispatch(changeQuantityAndTotalOrder1());
    }
    const handlerDetail = () => {
       navigate("/Product1/1");
    }
    const handlerBack = () => {
        navigate("/");
    }
    return(
       
            <Grid item  xs={3} md={3} lg={3} sm={3}  mt={3} mb={3}>
                <Card sx={{ width: prosCardWidth }}>
                    <CardContent>
                        <Typography variant="h5" component="div">
                        {mobileList[0].name}
                        </Typography>
                        <br/>
                        <Typography sx={{ m: 1.5 }} color="text.secondary">
                        Price: {mobileList[0].price}
                        </Typography>
                        <Typography sx={{ m: 1.5 }} color="text.secondary">
                        Quantity: {mobileList[0].quantity}
                        </Typography>
                        <Typography variant="body2">
                            <Button sx={{display: propsBuyDisplay, margin: "4px"}} variant="contained"size="small" onClick={() => ChangeonClickOrder1()}>Buy</Button>
                            <Button sx={{display:propsDetailDisplay , margin: "4px"}} variant="contained"size="small" onClick={handlerDetail} >Detail</Button>
                            <Button sx={{display:propsBackDisplay , margin: "4px"}} variant="contained"size="small"  onClick={handlerBack}>Back</Button>
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        
    )
}