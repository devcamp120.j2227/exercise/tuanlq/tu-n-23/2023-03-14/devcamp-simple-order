import { 
    CHANGE_QUANTITY_ADD_TOTAL,
    ON_CLICK_BUY_ORDER1,
    ON_CLICK_BUY_ORDER2,
    ON_CLICK_BUY_ORDER3

} from "../constants/card.constants";

// Nơi mô tả những sự kiện liên quan đến task (Chứa các hàm return về object)
export const changeQuantityAndTotal= (value) => {
    return {
        type: CHANGE_QUANTITY_ADD_TOTAL,
        payload: value
    }
}
export  const changeQuantityAndTotalOrder1 = () => {
    return {
        type: ON_CLICK_BUY_ORDER1,
    }
}
export const changeQuantityAndTotalOrder2 = () => {
    return {
        type: ON_CLICK_BUY_ORDER2,
    }
}

export const changeQuantityAndTotalOrder3 = () => {
    return {
        type: ON_CLICK_BUY_ORDER3,
    }
}

